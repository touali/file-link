# FileLink Block

FileLink provide a simple field formatter to display your files as link to their sources

## Usage

Install and enable this module the way you like.  
Remember that this block is usable on nodes.

### Composer install

Past this in your composer.json repositories 
    
    {
        "type": "vcs",
        "url": "https://touali@bitbucket.org/touali/file-link.git"
    }


And then
    
    composer require touali/file_link


## TODO

* Add translations.
* Check if there is a better way to display file size and a better place to process it.
* Figure out if we need to set dependencies
* Handle conditionnal fields with description checkboxe & label field
* A lot of things I have not thought about yet...
