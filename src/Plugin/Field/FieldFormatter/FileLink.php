<?php

namespace Drupal\file_link\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\file\Plugin\Field\FieldFormatter\GenericFileFormatter;

/**
 * Plugin implementation of the 'file_link' formatter.
 *
 * @FieldFormatter(
 *   id = "file_link",
 *   label = @Translation("Static link"),
 *   field_types = {
 *     "file"
 *   }
 * )
 */
class FileLink extends GenericFileFormatter
{
  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    $options = parent::defaultSettings();

    $options['link_label'] = t('Download');
    $options['download'] = false;
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['link_label'] = [
      '#title' => t('Link label'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('link_label')
    ];

    $elements['download'] = [
      '#title' => t('Force download'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('download')
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $label = $this->getSetting('use_description_as_link_text') ? 'Description field' : $this->getSetting('link_label');

    if (isset($label)) {
      $summary[] = t('Link label: @label', ['@label' => $label]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      assert($file instanceof FileInterface);
      $item = $file->_referringItem;

      $elements[$delta] = [
        '#theme' => 'file_link_formatter',
        '#url' => $file->createFileUrl(),
        '#size' => $file->getSize(),
        '#filename' => $file->getFilename(),
        '#label' => $this->getSetting('use_description_as_link_text') ? $item->description : $this->getSetting('link_label'),
        '#download' => $this->getSetting('download'),
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
        '#allowed_tags' => ['a'],
      ];
    }

    return $elements;
  }
}
